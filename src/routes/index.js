import {View, Text} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {PartSatu, Home} from '../pages';
const Navigasi = createNativeStackNavigator();

const Route = () => {
  return (
    <Navigasi.Navigator>
      <Navigasi.Screen name="Home" component={Home} />
      <Navigasi.Screen name="Part Satu" component={PartSatu} />
    </Navigasi.Navigator>
  );
};

export default Route;
